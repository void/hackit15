#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Hackmeeting'
SITENAME = u'Hackmeeting 0x12'
# SITEURL = 'http://hackmeeting.org/hackit15'

PATH = 'content'
PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['news']
STATIC_PATHS = ['images', 'talks']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'it'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = None
# Social widget
SOCIAL = None
DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

DISPLAY_CATEGORIES_ON_MENU = False
DEFAULT_DATE = (2015, 3, 1)
TYPOGRIFY = True

PAGE_ORDER_BY = 'navbar_sort'

PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'
PAGE_LANG_URL = '{slug}.{lang}.html'
PAGE_LANG_SAVE_AS = '{slug}.{lang}.html'

THEME = 'themes/bs3'

# Pelican bootstrap 3 theme settings
BOOTSTRAP_THEME = 'cyborg'

HIDE_SIDEBAR = True
PLUGIN_PATHS = ['plugins']
PLUGINS = ['langmenu']
