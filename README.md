Hackmeeting 2015
==================

This is the sources for Italian Hackmeeting 0x11 (2015). To develop, create a
python2 virtualenv and `pip install -r requirements.txt`.

After that, `make help` is your friend.
