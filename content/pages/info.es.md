Title: Info
Date: 2015-03-13 12:00
Slug: info
navbar_sort: 3
lang: es

## Donde
El Hackmeeting 2015 se realizará en el espacio okupado Mensa Occupata, en pleno centro de Nápoles. La dirección es "Via Mezzocannone".


## Presentación
El Hackmeeting 2015 se realizará en Nápoles del 19 al 21 de junio.



## Entrada 
Como todos los años, la entrada al Hackmeeting es completamente gratuita; pero aún así, acuérdate que la organización del Hackmeeting tiene un costo, y las suscripciones son necesarias para la supervivencia del evento!

Si piensas venir antes del viernes, ¡genial! En este caso pedimos que nos informes con antelación para que podamos organizarlo. 

## Donde quedarse

En el espacio okupado habrán salas donde puedes dormir y dejar tus cosas. Te sugerimos que traigas tu propio saco de dormir y / o esterilla. Por desgracia, en el centro de Nápoles, no hay hermosos prados verdes para acampar. De hecho, como dijimos el año pasado también, ¡no hay espacio incluso para la gente que solía venir en autocaravana! 

## Dónde y qué comer

Incluso la gestión de la cocina será autoorganizada y te animamos a colaborar. Como todos los años, vamos a respetar las necesidades de cada tipo de alimentación y por eso haremos comida vegana y vegetariana, sin gluten y así sucesivamente. Si tiene alguna preferencia particular, escríbela en la lista.

## ¿Qué puedo llevar?

Si estás planeando utilizar un laptop, traerlo junto con una regleta. No te olvides de un dispositivo de red de cualquier tipo (ver dispositivos, cables Ethernet, interruptores, y / o WiFi). Asimismo, recuerda a traer todo el hardware que querrás hackear junto con los demas y no se te olvide el material de dormir (saco de dormir, teclado en pantalla u otro). Durante el evento el Internet no estará garantizado, por tanto, si crees que lo necesitas, trae una Usb 3G y la necesidad de compartirla con los amigos! En general, trata de ser tecnologicamente autosuficiente.

## Que es un “warmup”

Los eventos de warmup son "preparatorios" para el hackmeeting. Se realizan en varios sitios de Italia, y se pueden manejar los más diversos temas. 

## Lista 

Por ahora estás en el [Wiki](https://lab.dyne.org/Hackmeeting2015/it/WarmUp).

## Proponer un warmup

 ¿Quieres hacer un warmup? excelente! Suscríbete a la lista de correo del hackmeeting. Escribe en lista de correo sobre tu evento: no hay necesidad de ninguna "aprobación oficial", pero repórtalo en la lista que sigue siendo un paso útil para promover el debate y la comunicación. Escribe sobre ti en el [Wiki](https://lab.dyne.org/Hackmeeting2015/it/WarmUp). El Wiki necesita autenticación, y se puede encontrar la contraseña en los [archivos de la lista de correo del hackmeeting](https://lists.autistici.org/list/hackmeeting.html).

## Que comportamiento se espera en Hackmeeting

Hackmeeting es un espacio autogestionado, una zona autónoma temporal y
quien pasa por ella es responsable de que la realización de las jornadas
se desarrollen en el respeto del antisexismo, antirracismo y
antifascismo. Si sufres o asistes a episodios de opresión, agresión,
fuerza bruta, escaneo de puertos, ping flood y otros DOS no consensuados
y no sabes como reaccionar o limitar el ataque, cuenta con el apoyo de
toda la comunidad y no dudes en llamar la atención y pedir ayuda.

##  Puedo hacer fotografías, grabar vídeos, publicar, etiquetar, compartir, subir los ficheros a internet?

Creemos que a cada participante tenga que ser garantizada la libertad de
elegir en autonomía la amplitud de su esfera privada y de sus perfiles
públicos; por esto dentro de hackmeeting está permitido hacer
fotografías y vídeos solo si  explícitamente señalados y previamente
autorizados por todas las personas que salen en ellas.


