Title: WarmUp
Date: 2015-04-23 10:00
Slug: warmup
navbar_sort: 4

#### Cosa sono

I warmup sono eventi "preparatori" ad hackmeeting. Avvengono in giro per l'italia, e possono trattare gli argomenti più disparati.

#### Elenco

Per ora lo trovi sul [Wiki](https://lab.dyne.org/Hackmeeting2015/it/WarmUp)

#### Proporre un warmup

Vuoi fare un warmup? ottimo! 

* iscriviti alla [mailing list di hackmeeting](https://www.autistici.org/mailman/listinfo/hackmeeting).
* scrivi in mailing list riguardo al tuo warmup: non c'è bisogno di alcuna "approvazione ufficiale", ma segnalarlo in lista è comunque un passaggio utile per favorire dibattito e comunicazione.
* segnalo tu stesso sul [Wiki](https://lab.dyne.org/Hackmeeting2015/it/WarmUp). Il wiki ha bisogno di autenticazione, e puoi trovare la password negli [archivi della mailing list di hackmeeting](https://lists.autistici.org/list/hackmeeting.html).
