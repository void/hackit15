Where
###################

:date: 2015-03-13 12:00
:slug: dove
:lang: en
:navbar_sort: 2

Hackmeeting 2015 will be at Mensa Occupata, an occupied space in the real
centre of Naples. The address is "via Mezzocannone"
