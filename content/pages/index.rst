:save_as: index.html
:slug: index
:navbar_sort: 1

"Ieri a est nel cielo,

e la luna a metà

all'altezza del tetto,

Venere splendeva.

All'alba lo sguardo

personale, sotto il bel manto

universale, apre le sue ali

lento risale a spirale

da qui all'altro capo

della terra (oltre il grande mare).

Nell'urlo aperto della vita

o strozzato dalla guerra

la gabbia appare uguale!

Stesso il colore,

stessi i toni,

non già le sfumature

e le infinite gradazioni.

Stessa è la fine 

che sia tra spine o fiori.

Se non ci accontentiamo

forse stesse son le favole,

stesse le illusioni."


**Non dire una parola che non sia d'amore. Ciao Lollo**