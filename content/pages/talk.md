Title: Programma
Date: 2015-06-4 10:00
Slug: programma
navbar_sort: 5

### Venerdì


| 		  | SALA RING         | SALA DOPOSCUOLA 					|
|---------|:---------------:|:-------------------------:|
|  11:30  | Assemblea 		|							|
| 		  |					| 							|
| 	15	  | [Ninux](#ninux) | [identità profili online](#profili) 	|
|	16	  | [TOR 4 beginners](#tor4beginners)	|       [larigira](#larigira)			|
|   17    | TOR	4 beginners	| [GNU/Linux Intro](#gnulinux)			|
| 	18 	  | Assemblea |	|
|   18:30    | 	| [Con-sensualità](#mani)							|
|         |					| 							|
|  21:30  |	[Freepto Contest](#freepto) | [Mappatura del rischio](#mappatura)		|

### Sabato

|  		  | SALA RING         		 |			SALA DOPOSCUOLA		      | SALA ACQUARIO |
|---------|:------------------------:|:-----------------------------:|:------:|
| 11      | Dimostrazione scherma medievale | Dimostrazione boxe |[Allenamento pre-sportantisessista](#allenamento) |
| 11:30   | 		 | | [Ippolita: Autodifesa neuro-digitale](#neuro)	 |
| 12	  | [10MIN FLASH ⚡TALK](#10min) | 	 							 | Ippolita: Autodifesa neuro-digitale| 
|         |						     | 								 |
| 15      | [OpenCV](#opencv) 		 | [Non solo sfiga](#sarzana)			 		     | 
| 16	  |   | Non solo sfiga  |
| 17 	  | [Libreant](#libreant)	 | [L'hArdomotica: arduino per la domotica](#hardomotica)  |[dockerizing your applications](#dockerizing) |
| 18      |  |	[Tavola Rotonda Sicurezza Digitale](#tavolasicurezza)		 			 |
|         | 						 |                               |
| 21:30   | [10MIN FLASH ⚡TALK](#10min)		 |								 |

### Lista seminari

#### <a name="10min"></a> 10MIN FLASH ⚡TALK
l'angolo delle persone pigre, modeste o timide o al contrario di chi cerca la fama facile. Di coloro che ritengono che quanto non riesci a sintetizzare in dieci minuti non ha senso dirlo, oppure che non hanno cosi' alta opionione di se' da pensare di essere interessanti per piu' di 10 minuti, oppure che sono troppo stanche per esserlo, oppure che semplicemente vogliono i loro dieci minuti di gloria e poi sono contente cosi'.

##### Sessione 1

+ __SD wifi__ ([SLIDE]({filename}/talks/boyska/sdwifi.html)):
SD cards con wifi incorporato e su cui gira Linux ([esempio](https://www.pitt-pladdy.com/blog/_20140202-083815_0000_Transcend_Wi-Fi_SD_Hacks_CF_adaptor_telnet_custom_upload_/))
+ __noscript e torbrowser__:
le particolarità della configurazione di default con cui NoScript viene incorporato nel Tor Browser, le caratteristiche specificamente implementate per il Tor Project e le motivazioni che ci stanno dietro
+ __Brop__ ([appunti]({filename}/talks/brop/appunti.txt)):
Quello che ho capito di Blind Return Oriented Programming
+ __Libmapper e accrocchio video__:
libreria per la mappatura real-time di sensori. Un'implementazione utile e intelligente di Open Sound Control.
+ __CNC / grbl / CAD / CAM__:
Una panoramica assolutamente non preparata riguardo esperimenti con un CNC autocostruito e la ricerca degli strumenti per fargli fare tanti giochini. (lesion) keywords: grbl, arduino, motori passo passo, CNC, CAD, CAM, pcb, esticazzi
+ __Linux Backdoors__: Una overview storica sulle backdoor su GNU/Linux

##### Sessione 2

+ __BioHacking__
+ __Introduzione ad OpenWRT__
+ __Lo smartphone liberato__
+ __Giocare con i "browser bot": python, selenium e oltre__:
([SLIDE]({filename}/talks/boyska/bot.html))
nel talk mostrero' come si usano alcune librerie utili a simulare la navigazione utente scrivendo un programma (roba semplice comunque); se riesco e se vi prende bene possiamo divagare sullo spinoso tema del "come gestire i dati raccolti" il quale e' un po' un panico


#### <a name="ninux"></a> Ninux

#### <a name="tor4beginners"></a> TOR 4 beginners

([Slide]({filename}/talks/tor4beginners/tor4beginners.tar.gz))

#### <a name="profili"></a> Gestione delle identità e dei profili online

Come e perche' separare le proprie identita' e i propri profili per evitare sia la profilazione che gli attacchi informatici. Dalla creazione di profili diversi sui browser all'uso di virtual machine per aprire allegati alla separazione dei propri domini personali sia a livello di social engineering che di sistema operativo (VM, Tails, Qubes).

#### <a name="mani"></a> Con-sensualità: mettici le mani

Questo workshop si propone di ridurre le barriere fisiche che ci separano quando interagiamo tramite tastiera, parlare di conenso e consensualità, e volendo dibattere un po' il famoso flame della policy e come ci si approccia nelle situazioni che dovrebbero essere liberate

#### <a name="freepto"></a> Freepto Contest

In questo talk vi convinceremo a sviluppare, diffondere, difendere [freepto](http://www.freepto.mx/) ([link](https://github.com/avana-bbs/freepto-lb)). Poi daremo inizio al [freepto contest](https://pad.riseup.net/p/r.f2689dfccb9815826e38a642645b928d) che durera' fino a domenica e vi assicuriamo che e' una roba bella assai 

#### <a name="mappatura"></a> Autogestire la mappatura del rischio

Laboratorio interattivo per ottenere piu' autonomia nella scelta degli strumenti che ciascuna di noi ritiene piu' sostenibili e utili per le sue esigenze personali a partire da una riflessione sul threat modeling (modellazione del pericolo). Alla fine del percorso chi ha partecipato dovrebbe avere un'idea di quali strumenti di sicurezza sono piu' adatti alle sue esigenze e alle sue risorse e di quali altri talk di sicurezza informatica gli interessa seguire ad hackit.

[link](http://www.cantiere.org/5470/cyber-resistance-back-10-e-11-aprile-al-cs)

#### <a name="neuro"></a> Autodifesa neuro-digitale - Neuro-Digital Self-Defence

Obiettivo dell'autodifesa è la valorizzazione delle differenze individuali e l'ampliamento dei propri gradi di libertà. Capire come funziona la cognizione digitale è un primo passo per vivere con le macchine. parleremo di: 1. Come è fatto il cervello: elementi di neuroscienze. 2. Cibernetica quotidiana: il feedback neuronale nell'interazione umano-macchina (schermo). 3. Circuiti di piacere, circuiti di dolore. Il sistema dopaminico tra ripetizione e rinforzo comportamentale 4. Assuefazioni mediatiche. 5. Memoria procedurale (intelligenza simultanea) e memoria dichiarativa (intelligenza sequenziale). 6. Neuromiti digitali: nativi e immigranti?

di [Ippolita](http://ippolita.net)

#### <a name="opencv"></a> Intro OpenCV

Una breve panoramica sulle [OpenCV](http://opencv.org/)

#### <a name="gnulinux"></a> GNU/Linux intro

Introduzione politico/tecnica al Software Libero

#### <a name="libreant"></a> Libreant

([SLIDE]({filename}/talks/libreant/index.html))
Un software per l'archiviazione e la gestione di libri, pensato per coniugare librerie cartacee e digitali. La struttura del db e' tale da poter archiviare differenti tipi di testi con differenti metadati. Punto di forza del progetto e' la possibilita' di federare piu' nodi
[documentazione](https://libreant.rtfd.org) [codice](https://github.com/insomnia-lab/libreant)


#### <a name="larigira"></a> larigira: un radio automation coso

([SLIDE]({filename}/talks/boyska/larigira.html))

Stato del free software "per radio", motivazioni, dettagli tecnici su questo nuovo coso
[link](https://larigira.readthedocs.org/) [link](https://github.com/boyska/larigira)

#### <a name="dockerizing"></a> dockerizing your applications

([SLIDE]({filename}/talks/docker/dockerslides.tar.gz), [ESEMPI]({filename}/talks/docker/dockerexamples.tar.gz))

Introduzione all'uso e panoramica del funzionamento di docker... iniziamo a incapsulare i nostri programmi

#### <a name="tavolasicurezza"></a> Come fare formazione alla sicurezza digitale (senza la spiacevole sensazione di aver buttato parole al vento)

Tavola rotonda -- Una chiacchierata su cosa non funziona dell'approccio "ora vengo nel vostro centro sociale e vi insegno pgp", non solo dal punto di vista degli strumenti, ma anche nell'ottica dell'analisi del rischio de' partecipanti, di come funziona l'apprendimento degli adulti, e dell'autogestione della propria sicurezza, sia digitale che personale/emotiva.

#### <a name="sarzana"></a> Non solo sfiga: chiacchierate legali

Seminario con l'avvocato Fulvio Sarzana, in cui si chiacchiererà di AGCOM, DDL antiterrorismo, riforma UE su copyright, riciccescion ACTA in TTIP, e varie ed eventuali.

#### <a name="allenamento"></a> Allenamento pre-sportantisessista

In vista della tre giorni di sport antisessista che si terrà a Torino, un allenamento collettivo con tecniche di autodifesa DAL digitale per sentirci meglio e iniziare la giornata con più gusto

#### <a name="hardomotica"></a> L'hArdomotica: arduino per la domotica

([CODICE]({filename}/talks/harduino/harduino.zip))
le applicazioni di arduino per la domotica, sia nel codice che nell'assemblaggio hardware.
Livello: principiante
