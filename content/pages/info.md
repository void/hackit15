Title: Info
Date: 2015-03-13 12:00
Slug: info
navbar_sort: 3


#### Ingresso

Come ogni anno, l'ingresso all'Hackmeeting è del tutto libero: ricordati però che organizzare l'Hackmeeting ha un costo e le sottoscrizioni sono necessarie per la sopravvivenza di questo evento!

#### Chi lo organizza?

L'hackmeeting è un momento annuale di incontro di una comunità che si riunisce intorno a una mailing list. Non esistono organizzatori e fruitori. Tutti possono iscriversi e partecipare all'organizzazione dell'evento, semplicemente visitando il sito [www.hackmeeting.org](www.hackmeeting.org/hackit15) ed entrando nella comunità.

#### Chi è un hacker?

Gli hackers sono persone curiose, che non accettano di non poter mettere le mani sulle cose. Che si tratti di tecnologia o meno gli hackers reclamano la libertà di sperimentare. Smontare tutto, e per poi rifarlo o semplicemente capire come funziona. Gli Hackers risolvono problemi e costruiscono le cose, credono nella libertà e nella condivisione. Non amano i sistemi chiusi. La forma mentis dell'hacker non è ristretta all'ambito del software-hacking: ci sono persone che mantengono un atteggiamento da hacker in ogni campo dell'esistente, spinti dallo stesso istinto creativo.

#### Chi tiene i seminari?

Chi ne ha voglia. Se qualcuno vuole proporre un seminario, non deve fare altro che proporlo in lista. Se la proposta piace, si calendarizza. Se non piace, si danno utili consigli per farla piacere.

#### Ma cosa si fa, a parte seguire i seminari?

Esiste un "lan-space", vale a dire un'area dedicata alla rete: ognuno arriva col proprio portatile e si può mettere in rete con gli altri. In genere in questa zona è facile conoscere altri partecipanti, magari per farsi aiutare a installare Linux, per risolvere un dubbio, o anche solo per scambiare quattro chiacchiere. L'hackmeeting è un open-air festival, un meeting, un hacking party, un momento di riflessione, un'occasione di apprendimento collettivo, un atto di ribellione, uno scambio di idee, esperienze, sogni, utopie.

#### Quanto costa l'ingresso? Come viene finanziato l'evento?

Come ogni anno, l’ingresso all’Hackmeeting è del tutto libero; ricordati però che organizzare l’Hackmeeting ha un costo. Le spese sono sostenute grazie ai contributi volontari, alla vendita di magliette e altri gadget e in alcuni casi all'introito del bar.

#### Dove dormire

Dentro la mensa occupata ci saranno delle stanze dove poter dormire e lasciare le proprie cose. Consigliamo di portarsi il proprio sacco a pelo e/o materassino. Purtroppo, al centro di Napoli, non ci sono dei bei prati verdi per poter campeggiare. Infatti, come dicemmo anche l'anno scorso, non c'è posto neanche per chi è abituato a venire in camper!
In caso di elevata affluenza, sarà disponibile per dormire anche uno spazio a circa 150mt dalla Mensa, con vari compagni che gestiranno l'accesso al luogo.

#### Dove e cosa mangiare

Anche la gestione della cucina è autorganizzata e pertanto ti invitiamo a collaborare. Come tutti gli anni, si rispetteranno le necessità alimentari di ogni tipo e quindi si farà cucina vegana e vegetariana, cucina senza glutine e così via. Se hai una certa necessità, scrivilo in lista.

#### Cosa posso portare

Se hai intenzione di utilizzare un computer, portalo accompagnato da una ciabatta elettrica. Non dimenticare una periferica di rete di qualche tipo (vedi cavi ethernet, switch e/o dispositivi WiFi). Ricordati inoltre di portare tutto l'hardware su cui vorrai smanettare con gli altri e non scordare il materiale per dormire (sacco a pelo, tastiera morbida o altro).
Durante l'evento non è garantita la connessione internet, dunque, se pensi di averne bisogno, portati una pennina 3G e il necessario per condividerla con gli amici! In generale, cerca di essere autosufficiente sul lato tecnologico.

#### Posso arrivare prima di venerdì?

Vuoi arrivare qualche giorno prima? Fantastico! Nei giorni precedenti ad hackmeeting ci sono sempre molte cose da fare (preparare l'infrastruttura di rete, preparare le sale seminari, e tanto altro!) quindi una mano è ben accetta. Ti chiediamo però di comunicarcelo in anticipo via email all'indirizzo `hackit2015 at autistici.org` in modo da poterci organizzare.

#### Come ci si aspetta che si comportino tutte e tutti

Hackmeeting è uno spazio autogestito, una zona temporaneamente autonoma e chi ci transita è responsabile che le giornate di hackit si svolgano nel rispetto dell'antisessismo, antirazzismo e antifascimo. Se subisci o assisti a episodi di oppressione, aggressione, brute force, port scan, ping flood e altri DOS non consensuali e non sai come reagire o mitigare l'attacco, conta sul sostegno di tutta la comunità e non esitare a richiamare pubblicamente l'attenzione e chiedere aiuto.

#### Posso scattare foto, girare video, postare, taggare, condividere, uploadare?

Pensiamo che a ogni partecipante debba essere garantita la libertÃ  di scegliere in autonomia l'ampiezza della propria sfera privata e dei propri profili pubblici; per questo all'interno di hackmeeting sono ammessi fotografie e/o video solo se chiaramente segnalati e precedentemente autorizzati da tutte quanti vi compaiono.