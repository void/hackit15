Title: Contatti
Date: 2015-06-7 14:00
Slug: contatti
navbar_sort: 8


#### Come contattare la comunità Hackmeeting

**Mailing List**
La comunità Hackmeeting ha una [lista di discussione](https://www.autistici.org/mailman/listinfo/hackmeeting) dove poter chiedere informazioni e seguire le attività della comunità.

**IRC**
Esiste anche un canale IRC (Internet Releay Chat) dove poter discutere e chiacchierare con tutti i membri della comunità: collegati al server irc.autistici.org ed entra nel canale #hackit99

**E-Mail**
E' possibile contattare direttamente il gruppo organizzativo locale all'indirizzo email `hackit2015 chiocciola autistici punto org`
