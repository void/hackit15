Title: Call for Papers
Date: 2015-05-17 23:00
Slug: call
navbar_sort: 6


#### Introduzione

Hackmeeting è giunto alla sua 18ima edizione e quest'anno si svolgerà a Napoli dal 19 al 21 giugno 2015.
Come sapete, Hackmeeting è un evento completamente autogestito dove non ci sono né organizzatori|trici, né fruitori|trici ma solo partecipanti.
Tre giorni di seminari, giochi, feste, dibattiti, scambi di idee e apprendimento collettivo, per analizzare assieme le tecnologie che utilizziamo quotidianamente, come queste cambiano e che stravolgimenti inducono sulle nostre vite reali e virtuali, quale ruolo possiamo rivestire nell'indirizzare questo cambiamento per liberarlo dal controllo di chi vuole monopolizzarne lo sviluppo, sgretolando i tessuti sociali e relegandoci in spazi virtuali sempre più stretti.

#### Tematiche

Quest'anno al centro dell'evento ci sono la repressione ed il controllo sociale tecnologico. Come funzionano? Come possiamo neutralizzarli, hackerarli e riusare le tecnologie in una chiave diversa? Ci sporcheremo le mani con i malware, la crittografia asimmetrica, i social network, le intercettazioni e tutto ciò che ogni partecipante porterà all'evento. E cmq se vuoi parlare d'altro va bene lo stesso, qualsiasi argomento trattato con attitudine acara e con la volonta' di metterci le mani sopra, e' in linea con l'hackmeeting. All'interno di hackit sono molto apprezzati anche i percorsi e seminari di base, introduttivi e di avvicinamento, quindi anche se non ti senti un super hacker, ma pensi di saper spiegare ad altri cose che hai faticato ad imparare, non farti abbattere dalla timidezza e dall'inedia: e' bello e utile condividere le conoscenze e aprire hackit ad ogni livello di esperienza.
Oltre ai soliti interventi, quest'anno proveremo ad organizzare delle tavole rotonde, in cui chiacchierare in maniera orizzontale e confrontarci sui progetti che abbiamo sviluppato durante l'anno, senza "lezioni frontali". Se vuoi condividere il tuo progetto e confrontarti con altri progetti simili, annuncialo in mailing list ed organizzalo con la comunità!

Non sei ancora sicuro se il tuo seminario va bene? Dai un'occhiata alle edizioni precedenti e ricorda: porta cio' che vuoi trovare!

* [seminari 2013](http://hackmeeting.org/hackit13/seminari.html)
* [seminari 2014](http://www.hackmeeting.org/hackit14/programma/)

#### Internazionalizzazione 

Crediamo che lo scambio di conoscenze non possa essere limitato dalle barriere linguistiche, soprattutto quando si parla di tecnologie e problematiche che ignorano e travalicano i confini geografici. Quest'anno vogliamo provare a facilitare la collaborazione proponendo traduzioni che rendano l'incontro più accogliente per persone di ogni provenienza geografica e per accogliere talk di chiunque voglia condividere le sue conoscenze pur non sapendo parlare in italiano.
Qui trovi la [Call per interpreti](https://lab.dyne.org/Hackmeeting2015/it/CallPerInterpreti)

#### Date 

 * Consegna degli abstract: 19/06/2015
 * Ammissione del talk: boh
 * Pubblicazione plan: 19/06/2015
 * Inizio evento: 19/06/2015
 * Fine evento: 21/06/2015

#### Linee guida 

Se vuoi partecipare con un seminario, iscriviti alla lista e proponi la tua idea e/o [pubblicala sul wiki](https://lab.dyne.org/Hackmeeting2015/Talks)  ([qui le credenziali per pubblicare](https://lists.autistici.org/message/20150402.104622.ab72598e.en.html)) specificando quando lo vorresti fare, di cosa vorresti parlare e quanto tempo durerebbe il tuo intervento. È tutto autogestito.
Ti invitiamo a comunicare con anticipo ciò che vuoi portare ad Hackmeeting, così sarà più facile organizzare
la distribuzione dei talk e la sua diffusione su Internet. Per maggiori informazioni consulta questa pagina:
[Come Proporre Talks](https://lab.dyne.org/Hackmeeting2015/it/ComeProporreTalks) e usa la [mailing list](https://www.autistici.org/mailman/listinfo/hackmeeting)

Per le slide o altri supporti, ti preghiamo di usare formati liberi: html, pdf, odp, o quello che preferisci.

L'audio è una componente importante nella divulgazione del seminario dopo l'evento. Sarà presente, pertanto, una postazione multimediale con la quale sarà possibile registrare il proprio intervento, previo consenso del relatore e dei partecipanti. Qualora non fosse disponibile tale postazione, o preferibile dall'autore del talk per qualche motivo, è possibile registrare in modo autonomo l'audio ed allegarlo al materiale del seminario. Si preferiscono fortemente formati audio open e con metadati. Per informazioni le chiavi/valore da utilizzare, si può prendere come esempio: [http://wiki.arkiwi.org/view/Metadati](http://wiki.arkiwi.org/view/Metadati)


