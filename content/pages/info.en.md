Title: Info
Date: 2015-03-13 12:00
Slug: info
navbar_sort: 3
lang: en

#### Entrance

As every year, entrance to Hackmeeting is completely free: remember, however, that organization of the Hackmeeting has a cost, and subscriptions are necessary for the subsistance of the event!

If you're planning to come before Friday, great! We ask, however, to inform us in advance so that we can organize it.

#### Where to stay

In the occupied space there will be rooms where you can sleep and leave your stuff. We suggest you to bring your own sleeping bag and / or mat. Unfortunately, in the center of Naples, there are no beautiful green parks to camp. In fact, as we said last year too, there is no space even for people who used to come in motorhome!

#### Where and what to eat

Even the management of the kitchen is self-organized and we encourage you to cooperate. As every year, we will respect the needs of each diet, therefore we will prepare vegan, vegetarian, gluten-free food and so on. If you have some special preference, write it in the list.

#### What can I bring

If you're planning to use a computer, bring it along with a power strip. Do not forget a network device of any kind (see Ethernet cables, switches, and / or WiFi devices). Also remember to bring all the hardware you want to hack with others and do not forget the material for sleep (sleeping bag, soft keyboard or other). During the event the Internet is not guaranteed, so, if you think you need it, you can bring a 3G pendrive and the need to share it with friends! In general, try to be tecnologically self-sufficient.

#### What is a warmup

The warmup events are "preparatory" to hackmeeting. They take place around Italy, and can handle the most diverse subjects.

#### Coming earlier

You want to come before the "official" hackmeeting beginning? Great! In the previous days, there's always something to do (setting up network infrastructure, preparing the seminar rooms, and much more!) so your help is really welcome. We just ask you to let us know of your coming so that we can be better organized.

#### List

For now you are on the [Wiki](https://lab.dyne.org/Hackmeeting2015/it/WarmUp)

#### Propose a warmup

You want to do a warmup? excellent!

Subscribe to the [mailing list of hackmeeting](https://www.autistici.org/mailman/listinfo/hackmeeting).
Write in mailing list about your warmup: there is no need for any "official approval", but report it in the list it's still a useful step to promote the debate and communication.
Tell us about you in the [Wiki](https://lab.dyne.org/Hackmeeting2015/it/WarmUp). The wiki needs authentication, and you can find the password in the [hackmeeting mailing list files](https://lists.autistici.org/list/hackmeeting.html).

#### How are people supposed to behave

Hackmeeting is a self-managed space, a temporarily autonomous zone, and
everybody who takes part in it has a responsibility in ensuring that everything that happens during the event respects the principles of antisexism, antiracism and antifascism.
In case you are targeted by or witness oppressive attitudes, personal attacks,
harassment, brute force, port scan, ping flood and other kinds of non-consesual
DOS and you don't know how to respond or mitigate the attack, count on the whole community's support and don't hesitate to publicly report the abuse and to ask for help.

#### Can I take pictures, record videos, post, tag, share, upload?

We think that every participant should have the freedom to choose the
breadth of their privacy and public profiles; therefore, at Hackmeeting taking pictures and/or videos is only accepted if this is clearly signalled and previously authorized by the people in the picture/video.

