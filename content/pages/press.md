Title: Press
Date: 2015-06-4 10:00
Slug: press
navbar_sort: 7

#### Conferenza stampa

Abbiamo fissato una conferenza stampa per la presentazione di Hackmeeting giovedì 18 giugno 2015 a partire dalle ore 12 presso il complesso universitario di Monte Sant'Angelo, sito in via Cinthia 21.


#### Grafiche

* [Manifesto (versione web)]({filename}/images/hm2015_web.jpg)
* [Banner (468x60)]({filename}/images/hm2015_banner_468x60.jpg)
