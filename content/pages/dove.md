Title: Come arrivare
Date: 2015-05-17 23:00
Slug: dove
navbar_sort: 2

# Come arrivare ad Hackmeeting 

**Con l'aereo: dall'aeroporto Capodichino (Napoli) a Piazza Garibaldi (Stazione centrale)**

Bus: [Orari da Capodichino](https://www.anm.it/Upload/RES/PDF/ORARI/Capodichino%202014%20GIUGNO.pdf)

**Con il treno: da Piazza Garibaldi (Stazione centrale) ad hackit (Mensa Occupata)**

  * A piedi (20-25 minuti): prendi la traversa Corso Umberto I, di fronte la stazione di Piazza Garibaldi e percorri un 1,5 Km; 
   al semaforo con incrocio in Via Mezzocannone svolta a   destra e percorri ancora 200m, alla tua destra troverai la Mensa Occupata.

  * Metro: prendi la linea 1 dalla stazione Garibaldi in direzione Università, scendi a Piazza Bovio, imbocca sulla sinistra Via Mezzocannone, 200 m ed eccoti arrivato! 

  * Bus: 
    * R2 fermata Piazza Bovio in direzione Corso Umberto I (Via S. Carlo);
    * 202 fermata Piazza Bovio in direzione Corso Umberto I (Via Medina), poi imbocca Via Mezzocannone, 200 m ed eccoci arrivati! 

**Con la macchina:**

  * dall'autostrada Salerno-Reggio Calabria, uscita in direzione **Napoli Centro e Porto:**
    * prendi Via Alessandro Volta, percorri 300m poi gira a sinistra in Via Amerigo Vespucci, continua su Via Marina / Via Nuova Marina per oltre 1 Km, 
      gira a destra in Via Porta di Massa e prosegui su Via Mezzocannone; **ATTENZIONE!!! C'E' LA ZTL ATTIVA SEMPRE!**

  * dall'autostrada Roma, uscita in direzione **Tangenziale, Napoli Centro, Capodichino, Secondigliano: **
    * Prendi l'A56, uscita in direzione Centro direzionale, Stazione Centrale, Corso Malta:
    * Prendi la direzione Centro Direzionale, Corso Malta (subito sulla destra dopo il casello):
    * Entrare in **Napoli**. Continua dritto in direzione di Corso Malta:
    * Continua su Corso Malta, poi gira a destra in Via Nuova Poggioreale e giunto alla rotonda di Piazza Nazionale prendi la 5° uscita in Calata Ponte di Casanova, 
      prosegui in Via Casanova, gira a sinistra in Piazza San Francesco di Paola. Continua su Piazza Enrico De Nicola poi gira a sinistra in Piazza Capuana, 
      prendi Via Alessandro Poerio poi gira a destra in Piazza Giuseppe Garibaldi, a questo punto, se non ti sei perso, 
      prendi Corso Umberto I / Rettifilo e continua per oltre 1 Km poi svolta a destra su Via Mezzocannone; **ATTENZIONE!!! C'E' LA ZTL ATTIVA SEMPRE!**


**Da Parcheggio ad hackit (Mensa Occupata)**

Parcheggiare in zona è difficile ma ci sono diversi parcheggi privati a pagamento, tra cui il parcheggio [Brin](https://www.anm.it/default.php?ids=16&) con cui stiamo cercando di concordare un'agevolazione:

  * da via Brin. Da lí si arriva alla mensa in diversi modi: 
    * A piedi (30-35 minuti): dirigiti verso Via Amerigo Vespucci e prosegui su Via Nuova Marina per oltre 2 Km, svolta a destra in Via Porta di Massa quindi 
      prosegui dritto in Via Mezzocannone, 200m ed ecco la mensa sulla destra.
    * A piedi (40-45 minuti): percorri Via Alessandro Volta in direzione Piazza Garibaldi e poi segui le stesse indicazioni specificate prima dalla Stazione Garibaldi...
    * Bus: 
        * C57 fermata Corso Umberto I, prosegui in direzione Via Amerigo Vespucci (Via Duomo) poi imbocca Via Mezzocannone, 200 m ed eccoci arrivati;
        * 192 fermata Piazza Garibaldi proseguendo in direzione Via Amerigo Vespucci (Via Salgari) quindi vedi le precedenti indicazioni per arrivare in Mensa

Agevolazione parcheggio Brin:
Per poter raggiungere un accordo il più possibilmente vantaggioso con il parcheggio Brin (che è privato, quindi a **pagamento**) abbiamo bisogno di capire quante in linea di massima possono essere le automobili da ospitare per l'evento.
Scriveteci ad `hackit2015 at autistici.org` così da potervi anche mandare il pass (da stampare) per poter ricevere l'agevolazione.
