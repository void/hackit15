About
###################

:save_as: about.html
:slug: about
:navbar_sort: 1

L'hackmeeting del 2015 si terrà a Napoli da venerdì 19 a domenica 21 giugno.

L'*hackmeeting* è l'incontro annuale delle controculture digitali italiane, di quelle comunità che si pongono in maniera critica rispetto ai meccanismi di sviluppo delle tecnologie all'interno della nostra società. Ma non solo, molto di più. Lo sussuriamo nel tuo orecchio e soltanto nel tuo, non devi dirlo a nessuno: l'hackit è solo per veri hackers, ovvero per chi vuole gestirsi la vita come preferisce e sa s/battersi per farlo. Anche se non ha mai visto un computer in vita sua.

Tre giorni di seminari, giochi, feste, dibattiti, scambi di idee e apprendimento collettivo, per analizzare assieme le tecnologie che utilizziamo quotidianamente, come cambiano e che stravolgimenti inducono sulle nostre vite reali e virtuali, quale ruolo possiamo rivestire nell'indirizzare questo cambiamento per liberarlo dal controllo di chi vuole monopolizzarne lo sviluppo, sgretolando i tessuti sociali e relegandoci in spazi virtuali sempre più stretti.

**L'evento è totalmente autogestito: non ci sono organizzatori e fruitori, ma solo partecipanti.**
